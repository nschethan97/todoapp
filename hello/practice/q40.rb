def string_test(str)
    str1 = ""
    str.split("").each_with_index do |char, index|
        str1 += char unless index % 2 == 1     
    end
    return str1
end
print string_test('abcdefgij'),"\n"
print string_test('abcdefg'),"\n"
print string_test('abcdef'),"\n"
print string_test('abc'),"\n"
print string_test('ab'),"\n"