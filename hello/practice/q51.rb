def check_num(a, b)
    a_digit = a%10
	b_digit = b%10;
	a /= 10;
	b /= 10;
	return (a == b || a == b_digit || a_digit == b || a_digit == b_digit)
end
print check_num(9, 12),"\n"
print check_num(15, 51),"\n"
print check_num(12, 23)