def check_num(a, b)
    if(a == b)
         return 0
     end	
     if(a % 5 == b % 5)
         return (a < b) ? a : b
     end
     return (a > b) ? a : b
 end
 
 print check_num(9, 12),"\n"
 print check_num(110, 200),"\n"
 print check_num(10, 10)