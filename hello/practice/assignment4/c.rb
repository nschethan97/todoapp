require 'crack' # XML and JSON parsingrequire 'crack/json' # Only JSON parsing
require 'crack/xml' # Only XML parsing

response = Crack::XML.parse(File.read('food.xml'))
# => {"menu"=>{"food"=>[{"name"=>"Waffles", "price"=>"5.95"}, {"name"=>"French Toast", "price"=>"4.50"}]}}

response = Crack::JSON.parse(File.read('food.json'))
# => {"menu"=>{"food"=>[{"name"=>"Waffles", "price"=>"5.95"}, {"name"=>"French Toast", "price"=>"4.50"}]}}