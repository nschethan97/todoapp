require 'rubygems'
require 'json'
require 'pp'
require 'gyoku'

json = File.read('input.json')
obj = JSON.parse(json)
one = Gyoku.xml(obj)
out_file = File.new ("xml_file.xml", "w+")
out_file.puts(one)
out_file.close