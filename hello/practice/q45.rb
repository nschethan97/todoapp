def check_num(a,b)
    return a == 11 || b == 11 || a + b == 11 || (a-b).abs == 11
 end
 
 print check_num(9, 11),"\n"
 print check_num(13, 7),"\n"
 print check_num(22, 11),"\n"
 print check_num(21, 32),"\n"
 print check_num(20, 30)