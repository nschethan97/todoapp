arra1 = ['abcde', 'abdf', 'adeab', 'abdgse', 'bdefa', 'bacdef']
print "Original array:\n"
print arra1
print "\nSearch items start with 'ab':\n"
print arra1.grep(/^ab/)
print "\nSearch items start with 'b':\n"
print arra1.grep(/^b/)