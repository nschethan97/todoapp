def check_array(nums)
    found1 = false    
    i = 0
    while i < nums.length
        if(nums[i] == 2)
             found1 = true
         end            
         if(found1 && nums[i] == 3)
             return true
         end  
         i = i + 1
    end
    return false
 end
 print check_array([6, 2, 3, 5]),"\n"
 print check_array([2, 6, 5, 3]),"\n"
 print check_array([6, 2, 5, 3]),"\n"
 print check_array([3, 6, 5, 2]),"\n"
 print check_array([3, 3, 2, 1]),"\n"