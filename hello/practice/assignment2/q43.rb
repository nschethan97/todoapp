nums = [10, 20, 30, 40, 10, 10, 20]
print "Original array:\n"
print nums
nums_freq = nums.inject(Hash.new(0)) { |h,v| h[v] += 1; h }
print "\nFrequency of numbers:\n"
print nums_freq