num1 = [10, 20, 30, 40, 10, 10, 20]
print "Original array:\n"
print num1
print "\nIs all items are identical?\n"
print num1.all? {|x| x == num1[0]}
num2 = [10, 10, 10]
print "\nOriginal array:\n"
print num2
print "\nIs all items are identical?\n"
print num2.all? {|x| x == num2[0]}