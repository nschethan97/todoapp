def check_num(a, b, c)
    return ((a - b).abs >= 20 || (b - c).abs >= 20 || (c - a).abs >= 20)
end

print check_num(9, 12, 22),"\n"
print check_num(112, 202, 20),"\n"
print check_num(102, 203, 405)