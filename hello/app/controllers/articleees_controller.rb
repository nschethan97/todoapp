class ArticleeesController < ApplicationController
  before_action :set_articleee, only: [:show, :edit, :update, :destroy]

  # GET /articleees
  # GET /articleees.json
  def index
    @articleees = Articleee.all
  end

  # GET /articleees/1
  # GET /articleees/1.json
  def show
  end

  # GET /articleees/new
  def new
    @articleee = Articleee.new
  end

  # GET /articleees/1/edit
  def edit
  end

  # POST /articleees
  # POST /articleees.json
  def create
    @articleee = Articleee.new(articleee_params)

    respond_to do |format|
      if @articleee.save
        format.html { redirect_to @articleee, notice: 'Articleee was successfully created.' }
        format.json { render :show, status: :created, location: @articleee }
      else
        format.html { render :new }
        format.json { render json: @articleee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articleees/1
  # PATCH/PUT /articleees/1.json
  def update
    respond_to do |format|
      if @articleee.update(articleee_params)
        format.html { redirect_to @articleee, notice: 'Articleee was successfully updated.' }
        format.json { render :show, status: :ok, location: @articleee }
      else
        format.html { render :edit }
        format.json { render json: @articleee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articleees/1
  # DELETE /articleees/1.json
  def destroy
    @articleee.destroy
    respond_to do |format|
      format.html { redirect_to articleees_url, notice: 'Articleee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_articleee
      @articleee = Articleee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def articleee_params
      params.require(:articleee).permit(:title, :descrption)
    end
end
