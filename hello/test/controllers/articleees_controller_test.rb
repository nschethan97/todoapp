require 'test_helper'

class ArticleeesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @articleee = articleees(:one)
  end

  test "should get index" do
    get articleees_url
    assert_response :success
  end

  test "should get new" do
    get new_articleee_url
    assert_response :success
  end

  test "should create articleee" do
    assert_difference('Articleee.count') do
      post articleees_url, params: { articleee: { descrption: @articleee.descrption, title: @articleee.title } }
    end

    assert_redirected_to articleee_url(Articleee.last)
  end

  test "should show articleee" do
    get articleee_url(@articleee)
    assert_response :success
  end

  test "should get edit" do
    get edit_articleee_url(@articleee)
    assert_response :success
  end

  test "should update articleee" do
    patch articleee_url(@articleee), params: { articleee: { descrption: @articleee.descrption, title: @articleee.title } }
    assert_redirected_to articleee_url(@articleee)
  end

  test "should destroy articleee" do
    assert_difference('Articleee.count', -1) do
      delete articleee_url(@articleee)
    end

    assert_redirected_to articleees_url
  end
end
